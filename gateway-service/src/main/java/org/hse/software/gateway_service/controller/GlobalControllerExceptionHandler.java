package org.hse.software.gateway_service.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.reactive.function.client.WebClientResponseException;

@RestControllerAdvice
@Component
public class GlobalControllerExceptionHandler {

    @ExceptionHandler({WebClientResponseException.class})
    public ResponseEntity<String> handleWebErrorRequest(WebClientResponseException ex) {
        return new ResponseEntity<>(ex.getResponseBodyAsString(), ex.getStatusCode());
    }
}
