package org.hse.software.gateway_service.filter;

import lombok.NoArgsConstructor;
import org.hse.software.gateway_service.validator.SecuredValidator;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;


@Component
public class AuthenticationPrefilter extends AbstractGatewayFilterFactory<AuthenticationPrefilter.Config> {
    private final WebClient.Builder webClientBuilder;
    private final org.springframework.cloud.client.discovery.DiscoveryClient discoveryClient;

    private final SecuredValidator securedValidator;

    public AuthenticationPrefilter(SecuredValidator securedValidator, WebClient.Builder webClientBuilder, org.springframework.cloud.client.discovery.DiscoveryClient discoveryClient) {
        this.securedValidator = securedValidator;
        this.webClientBuilder = webClientBuilder;
        this.discoveryClient = discoveryClient;
    }

    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            ServerHttpRequest request = exchange.getRequest();
            String jwtTokenId = request.getHeaders().getFirst("Authorization");
            if (jwtTokenId != null && securedValidator.isSecured.test(request)) {
                List<ServiceInstance> instances = discoveryClient.getInstances("authentication-service");
                if (instances.isEmpty()) {
                    return chain.filter(exchange);
                }
                ServiceInstance instance = instances.get(0);
                String serviceUri = String.format("%s://%s:%s", instance.getScheme(), instance.getHost(), instance.getPort());
                return webClientBuilder.build()
                        .get()
                        .uri(serviceUri + "/validate-token")
                        .header("Authorization", jwtTokenId)
                        .retrieve()
                        .bodyToMono(String.class)
                        .flatMap(jwtToken -> {
                            ServerHttpRequest verifiedRequest = request.mutate()
                                    .header("Authorization", "Bearer " + jwtToken)
                                    .build();
                            return chain.filter(exchange.mutate().request(verifiedRequest).build());
                        });
            } else {
                return chain.filter(exchange);
            }
        };
    }

    @NoArgsConstructor
    public static class Config {
    }
}
