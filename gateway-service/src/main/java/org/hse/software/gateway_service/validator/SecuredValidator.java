package org.hse.software.gateway_service.validator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;

@Component
public class SecuredValidator {
    private List<String> excludedUrls;

    public SecuredValidator(@Value("${gateway.excluded-urls}") String stringExcludedUrls) {
        excludedUrls = List.of(stringExcludedUrls.split(","));
    }

    public Predicate<ServerHttpRequest> isSecured =
            request -> excludedUrls
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));
}
