package org.hse.software.ru.ticket_service.model;

public enum OrderStatus {
    CHECK,
    SUCCESS,
    REJECTION
}
