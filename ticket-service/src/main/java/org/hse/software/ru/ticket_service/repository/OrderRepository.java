package org.hse.software.ru.ticket_service.repository;

import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.model.OrderStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findOrdersByStatus(OrderStatus status);
}
