package org.hse.software.ru.ticket_service.repository;

import org.hse.software.ru.ticket_service.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StationRepository extends JpaRepository<Station, Long> {
    boolean existsStationByName(String stationName);
    Station findStationByName(String stationName);
}
