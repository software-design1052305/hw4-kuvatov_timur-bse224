package org.hse.software.ru.ticket_service.controller;

import jakarta.validation.Valid;
import org.hse.software.ru.ticket_service.model.Station;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

public interface OrderControllerInterface {
    @PostMapping("order/create")
    public ResponseEntity<String> createOrder(@Valid @RequestBody OrderIn orderIn);
    @PostMapping("add/station")
    public ResponseEntity<String> addStation(@RequestBody StationIn stationIn);
}
