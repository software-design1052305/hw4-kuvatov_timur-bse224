package org.hse.software.ru.ticket_service.controller;

import jakarta.validation.Valid;
import org.hse.software.ru.ticket_service.model.Order;
import org.hse.software.ru.ticket_service.schema.OrderIn;
import org.hse.software.ru.ticket_service.schema.StationIn;
import org.hse.software.ru.ticket_service.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class OrderController implements OrderControllerInterface {
    private final OrderService orderService;

    private OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping("order/create")
    public ResponseEntity<String> createOrder(@Valid @RequestBody OrderIn orderIn) {
        Order order = orderService.createOrder(orderIn);
        return new ResponseEntity<>("{\"id\":" + order.getId() + "}", HttpStatus.OK);
    }

    @PostMapping("add/station")
    public ResponseEntity<String> addStation(@RequestBody StationIn stationIn) {
        try {
            orderService.addStation(stationIn);
            return new ResponseEntity<>("Success", HttpStatus.OK);
        }catch (IllegalArgumentException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Reason:" + exception.getMessage());
        }
    }

    @GetMapping("order/get/{id}")
    public ResponseEntity<Order> getOrder(@PathVariable(name = "id") Long orderId) {
        Order order;
        try {
            order = orderService.getOrder(orderId);
            return new ResponseEntity<>(order, HttpStatus.OK);
        } catch (IllegalArgumentException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Reason:" + exception.getMessage());
        }
    }
}
