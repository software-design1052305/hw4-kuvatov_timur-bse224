package org.hse.software.ru.ticket_service.schema;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hse.software.ru.ticket_service.annotation.StationExists;

@Data
public class OrderIn {
    @NotNull
    private Long userId;
    @NotBlank
    @StationExists(message = "From station name does not exists")
    private String fromStationName;
    @NotBlank
    @StationExists(message = "To station name does not exists")
    private String toStationName;
}
