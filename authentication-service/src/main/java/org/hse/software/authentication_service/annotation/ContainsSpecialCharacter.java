package org.hse.software.authentication_service.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.hse.software.authentication_service.validator.ContainsSpecialCharacterValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = ContainsSpecialCharacterValidator.class)
public @interface ContainsSpecialCharacter {
    String message() default "Must contains a special character.";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
