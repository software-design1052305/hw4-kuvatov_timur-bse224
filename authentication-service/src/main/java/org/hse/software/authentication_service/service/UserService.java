package org.hse.software.authentication_service.service;

import org.hse.software.authentication_service.model.User;
import org.hse.software.authentication_service.schema.LoginDetails;
import org.hse.software.authentication_service.schema.RegistrationDetails;

public interface UserService {
    void registerNewUser(RegistrationDetails registrationDetails);

    String login(LoginDetails loginDetails);

    User getUserInfo(String jwtToken);

    String validateToken(String jwtId);
}