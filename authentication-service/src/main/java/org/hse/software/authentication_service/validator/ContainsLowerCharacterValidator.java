package org.hse.software.authentication_service.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.hse.software.authentication_service.annotation.ContainsLowerCharacter;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.PasswordData;
import org.passay.PasswordValidator;

import java.util.List;

public class ContainsLowerCharacterValidator implements ConstraintValidator<ContainsLowerCharacter, String> {

    @Override
    public boolean isValid(String text, ConstraintValidatorContext context) {
        PasswordValidator validator = new PasswordValidator(List.of(
                new CharacterRule(EnglishCharacterData.LowerCase, 1)
        ));
        return validator.validate(new PasswordData(text)).isValid();
    }
}
