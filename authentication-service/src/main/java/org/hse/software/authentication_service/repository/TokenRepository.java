package org.hse.software.authentication_service.repository;

import org.hse.software.authentication_service.model.Token;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends KeyValueRepository<Token, String> {
}
