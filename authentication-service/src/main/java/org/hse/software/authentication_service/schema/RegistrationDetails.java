package org.hse.software.authentication_service.schema;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.hse.software.authentication_service.annotation.*;
import org.hse.software.authentication_service.model.AuthorityType;

@Data
@Setter
@Getter
public class RegistrationDetails {
    @Size(min = 3, max = 50, message = "The length of the nickname must be from 3 to 50")
    private String nickname;
    @Size(min = 3, max = 50, message = "The length of the password must be from 8 to 50")
    @ContainsUpperCharacter(message = "Password must contains upper character.")
    @ContainsLowerCharacter(message = "Password must contains lower character.")
    @ContainsDigit(message = "Password must contains digit.")
    @ContainsSpecialCharacter(message = "Password must contains special character.")
    private String password;
    @Pattern(regexp = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$",
            message = "Incorrect email.")
    @NotEmailAlreadyExists
    @Schema(
            example = "email@gmail.com"
    )
    private String email;
    @Size(max = 50, message = "The length of the code must be to 50")
    private String roleCode;

    @JsonIgnore
    private AuthorityType role = AuthorityType.DEFAULT;
}
