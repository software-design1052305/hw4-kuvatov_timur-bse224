package org.hse.software.authentication_service.annotation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import org.hse.software.authentication_service.validator.NotEmailAlreadyExistsValidator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = NotEmailAlreadyExistsValidator.class)
public @interface NotEmailAlreadyExists {
    String message() default "A user with such an email has already been registered";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}