package org.hse.software.authentication_service.schema;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class LoginDetails {
    private String email;
    private String password;
}
